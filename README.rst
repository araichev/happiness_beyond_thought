Happiness Beyond Thought
************************

In this eight-week course we will practice switching off the self-referential internal narrative (SRIN), the mind's press secretary, the i-me-my thoughts, the inner critic, the blah blah blah.
This is a good thing, because the SRIN causes all our suffering.
Moreover, switching off the SRIN has only upsides, e.g. increased cognitive performance and a sense that all is one and everything is now in an ever-changing dance of wonder and delight.
Those are bold claims, and we will test their validity throughout the course.

How to switch off the SRIN?
We will proceed kindly and gently with combinations of meditation, self-enquiry, posture flows, chanting, negations, and affirmations along the lines of Gary Weber's book *Happiness Beyond Thought*, which i like.

A persistent shut down of the SRIN may take 10,000 hours of practice, but i bet we will notice benefits within eight weeks.


Format
======
- Eight weekly 90-minute sessions of instruction, practice, and discussion Mondays 19:00--20:30 at Alex's house
- Bring your own meditation cushion and meditation/yoga mat, which you can leave on site
- Daily homework consisting of two 20-minute practices


Cost
====
Zero dollars and all of your personal attachments.


Teacher
=======
Alex Raichev, keen meditator and talker on the matter, not enlightened but working on it.


Resources
=========
- `Happiness Beyond Thought <Webe2007_book.pdf>`_ by Gary Weber
- Gary Weber's `videos on YouTube <https://www.youtube.com/user/gudakesha7/videos>`_


Week 0
======
Homework:

1. Find a meditation cushion to bring or a stack of firm pillows
2. Find a meditation/yoga mat to bring or a folded blanket
3. Download the book and get ready to read it